-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2020 at 01:07 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `guest-posting-portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `link`, `title`, `description`, `created_at`, `updated_at`, `price`) VALUES
(4, 'https://www.google.com/', 'title1', '<p>this is description</p>', '2019-07-29 11:05:41', '2019-07-31 06:54:51', 46),
(5, 'https://medium.com/', 'Medium', '<p>Testing</p>', '2019-12-18 04:22:36', '2019-12-18 04:22:36', 200),
(6, 'facebook.com', 'ABC', '<p>ABC<br></p>', '2019-12-18 06:16:07', '2019-12-18 06:16:07', 32);

-- --------------------------------------------------------

--
-- Table structure for table `blog_industries`
--

CREATE TABLE `blog_industries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `blog_id` bigint(20) UNSIGNED NOT NULL,
  `industry_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_industries`
--

INSERT INTO `blog_industries` (`id`, `blog_id`, `industry_id`, `created_at`, `updated_at`) VALUES
(2, 4, 1, '2019-12-18 02:46:36', '2019-12-18 02:46:36'),
(3, 5, 1, '2019-12-18 04:22:36', '2019-12-18 04:22:36'),
(4, 6, 1, '2019-12-18 06:16:07', '2019-12-18 06:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `blog_keywords`
--

CREATE TABLE `blog_keywords` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `blog_id` bigint(20) UNSIGNED NOT NULL,
  `keyword_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_keywords`
--

INSERT INTO `blog_keywords` (`id`, `blog_id`, `keyword_id`, `created_at`, `updated_at`) VALUES
(8, 4, 1, '2019-12-18 02:46:36', '2019-12-18 02:46:36'),
(9, 4, 2, '2019-12-18 02:46:36', '2019-12-18 02:46:36'),
(10, 5, 1, '2019-12-18 04:22:36', '2019-12-18 04:22:36'),
(11, 5, 2, '2019-12-18 04:22:36', '2019-12-18 04:22:36'),
(12, 6, 1, '2019-12-18 06:16:07', '2019-12-18 06:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `blog_metas`
--

CREATE TABLE `blog_metas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `blog_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_metas`
--

INSERT INTO `blog_metas` (`id`, `blog_id`, `meta_key`, `meta_value`, `created_at`, `updated_at`) VALUES
(6, 4, 'da', '102', NULL, '2019-12-18 02:46:36'),
(7, 4, 'fb_likes', '10', NULL, '2019-12-18 02:46:36'),
(8, 4, 'follower', '10', NULL, '2019-12-18 02:46:36'),
(9, 4, 'dropped', 'true', NULL, '2019-12-18 02:46:36'),
(10, 4, 'flag', 'true', NULL, '2019-07-29 11:05:51'),
(11, 5, 'da', '20', NULL, NULL),
(12, 5, 'fb_likes', '200', NULL, NULL),
(13, 5, 'follower', '1998', NULL, NULL),
(14, 5, 'dropped', 'false', NULL, NULL),
(15, 6, 'da', '21', NULL, NULL),
(16, 6, 'fb_likes', '323', NULL, NULL),
(17, 6, 'follower', '33', NULL, NULL),
(18, 6, 'dropped', 'false', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `credit_histories`
--

CREATE TABLE `credit_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

CREATE TABLE `industries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `industry` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`id`, `industry`, `created_at`, `updated_at`) VALUES
(1, 'Information Technology', '2019-07-31 09:50:19', '2019-07-31 09:50:19');

-- --------------------------------------------------------

--
-- Table structure for table `keywords`
--

CREATE TABLE `keywords` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `keywords`
--

INSERT INTO `keywords` (`id`, `keyword`, `created_at`, `updated_at`) VALUES
(1, 'Cars', '2019-07-30 10:47:07', '2019-07-30 10:47:07'),
(2, 'Shopify', '2019-12-18 02:45:38', '2019-12-18 02:45:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_07_19_105232_create_blogs_table', 1),
(4, '2019_07_19_105519_create_blog_metas_table', 1),
(5, '2019_07_19_105618_create_keywords_table', 1),
(6, '2019_07_19_113108_create_user_metas_table', 1),
(7, '2019_07_19_113601_create_blog_keywords_table', 1),
(8, '2019_07_19_115159_create_user_blogs_table', 1),
(9, '2019_07_19_115454_create_orders_table', 1),
(10, '2019_07_19_120831_create_order_blogs_table', 1),
(11, '2019_07_19_120933_create_credit_histories_table', 1),
(12, '2019_07_22_085655_create_permission_tables', 1),
(13, '2019_07_31_114717_add_price_to_blog_table', 2),
(14, '2019_07_31_114907_add_total_amount_to_orders_table', 2),
(15, '2019_07_31_132235_add_status_to_orders_table', 3),
(16, '2019_07_31_140818_create_industry_table', 4),
(19, '2019_07_31_140901_create_blog_industry_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `notifiable_id` int(200) NOT NULL,
  `data` longtext NOT NULL,
  `notifiable_type` longtext NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_id`, `data`, `notifiable_type`, `read_at`, `created_at`, `updated_at`) VALUES
('0d0d6553-ecf2-4347-af1f-d892a1efc2d3', 'App\\Notifications\\CreateOrders', 1, '{\"data\":{\"user_id\":14,\"user_meta\":\"a:10:{s:6:\\\"_token\\\";s:40:\\\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\\\";s:5:\\\"fname\\\";N;s:5:\\\"lname\\\";N;s:7:\\\"address\\\";N;s:4:\\\"city\\\";N;s:7:\\\"country\\\";N;s:9:\\\"post_code\\\";N;s:12:\\\"phone_number\\\";N;s:5:\\\"email\\\";s:23:\\\"ahsan.amin334@gmail.com\\\";s:6:\\\"amount\\\";s:2:\\\"32\\\";}\",\"status\":\"pending\",\"total_amount\":32,\"order_details\":\"a:1:{i:0;a:10:{s:5:\\\"title\\\";s:3:\\\"ABC\\\";s:7:\\\"web_url\\\";s:12:\\\"facebook.com\\\";s:11:\\\"description\\\";s:14:\\\"<p>ABC<br><\\/p>\\\";s:5:\\\"price\\\";s:2:\\\"32\\\";s:2:\\\"da\\\";s:2:\\\"21\\\";s:2:\\\"fb\\\";s:3:\\\"323\\\";s:8:\\\"follower\\\";s:2:\\\"33\\\";s:10:\\\"blog_image\\\";N;s:7:\\\"blog_id\\\";s:1:\\\"6\\\";s:7:\\\"user_id\\\";s:2:\\\"14\\\";}}\",\"user_email\":\"ahsan.amin334@gmail.com\",\"updated_at\":\"2020-01-14 13:13:48\",\"created_at\":\"2020-01-14 13:13:48\",\"id\":178}}', 'App\\User', '2020-01-14 10:38:34', '2020-01-14 08:13:48', '2020-01-14 10:38:34'),
('2cfec6da-6366-471b-8e4f-67df1166fad8', 'App\\Notifications\\CreateOrders', 1, '{\"data\":{\"user_id\":14,\"user_meta\":\"a:10:{s:6:\\\"_token\\\";s:40:\\\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\\\";s:5:\\\"fname\\\";s:5:\\\"Yasin\\\";s:5:\\\"lname\\\";s:4:\\\"amin\\\";s:7:\\\"address\\\";s:7:\\\"testing\\\";s:4:\\\"city\\\";s:6:\\\"kaghan\\\";s:7:\\\"country\\\";s:14:\\\"\\u00c5land Islands\\\";s:9:\\\"post_code\\\";s:6:\\\"354354\\\";s:12:\\\"phone_number\\\";s:7:\\\"0423423\\\";s:5:\\\"email\\\";s:23:\\\"ahsan.amin334@gmail.com\\\";s:6:\\\"amount\\\";s:2:\\\"32\\\";}\",\"status\":\"pending\",\"total_amount\":32,\"order_details\":\"a:1:{i:0;a:10:{s:5:\\\"title\\\";s:3:\\\"ABC\\\";s:7:\\\"web_url\\\";s:12:\\\"facebook.com\\\";s:11:\\\"description\\\";s:14:\\\"<p>ABC<br><\\/p>\\\";s:5:\\\"price\\\";s:2:\\\"32\\\";s:2:\\\"da\\\";s:2:\\\"21\\\";s:2:\\\"fb\\\";s:3:\\\"323\\\";s:8:\\\"follower\\\";s:2:\\\"33\\\";s:10:\\\"blog_image\\\";N;s:7:\\\"blog_id\\\";s:1:\\\"6\\\";s:7:\\\"user_id\\\";s:2:\\\"14\\\";}}\",\"user_email\":\"ahsan.amin334@gmail.com\",\"updated_at\":\"2020-01-14 15:41:00\",\"created_at\":\"2020-01-14 15:41:00\",\"id\":187}}', 'App\\User', '2020-01-14 10:41:23', '2020-01-14 10:41:00', '2020-01-14 10:41:23'),
('31558f2f-4818-4429-af50-1906672e6e88', 'App\\Notifications\\CreateOrders', 1, '{\"data\":{\"user_id\":14,\"user_meta\":\"a:10:{s:6:\\\"_token\\\";s:40:\\\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\\\";s:5:\\\"fname\\\";s:5:\\\"Yasin\\\";s:5:\\\"lname\\\";s:4:\\\"amin\\\";s:7:\\\"address\\\";s:7:\\\"testing\\\";s:4:\\\"city\\\";s:6:\\\"kaghan\\\";s:7:\\\"country\\\";s:14:\\\"\\u00c5land Islands\\\";s:9:\\\"post_code\\\";s:6:\\\"354354\\\";s:12:\\\"phone_number\\\";s:7:\\\"0423423\\\";s:5:\\\"email\\\";s:23:\\\"ahsan.amin334@gmail.com\\\";s:6:\\\"amount\\\";s:2:\\\"32\\\";}\",\"status\":\"pending\",\"total_amount\":32,\"order_details\":\"a:1:{i:0;a:10:{s:5:\\\"title\\\";s:3:\\\"ABC\\\";s:7:\\\"web_url\\\";s:12:\\\"facebook.com\\\";s:11:\\\"description\\\";s:14:\\\"<p>ABC<br><\\/p>\\\";s:5:\\\"price\\\";s:2:\\\"32\\\";s:2:\\\"da\\\";s:2:\\\"21\\\";s:2:\\\"fb\\\";s:3:\\\"323\\\";s:8:\\\"follower\\\";s:2:\\\"33\\\";s:10:\\\"blog_image\\\";N;s:7:\\\"blog_id\\\";s:1:\\\"6\\\";s:7:\\\"user_id\\\";s:2:\\\"14\\\";}}\",\"user_email\":\"ahsan.amin334@gmail.com\",\"updated_at\":\"2020-01-14 15:40:59\",\"created_at\":\"2020-01-14 15:40:59\",\"id\":186}}', 'App\\User', '2020-01-14 10:41:24', '2020-01-14 10:40:59', '2020-01-14 10:41:24'),
('3b1ce7c3-2d8a-4949-aec6-2b10d4b15638', 'App\\Notifications\\CreateOrders', 1, '{\"data\":{\"user_id\":14,\"user_meta\":\"a:10:{s:6:\\\"_token\\\";s:40:\\\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\\\";s:5:\\\"fname\\\";s:5:\\\"Yasin\\\";s:5:\\\"lname\\\";s:4:\\\"amin\\\";s:7:\\\"address\\\";s:7:\\\"testing\\\";s:4:\\\"city\\\";s:6:\\\"kaghan\\\";s:7:\\\"country\\\";s:14:\\\"\\u00c5land Islands\\\";s:9:\\\"post_code\\\";s:6:\\\"354354\\\";s:12:\\\"phone_number\\\";s:7:\\\"0423423\\\";s:5:\\\"email\\\";s:23:\\\"ahsan.amin334@gmail.com\\\";s:6:\\\"amount\\\";s:2:\\\"32\\\";}\",\"status\":\"pending\",\"total_amount\":32,\"order_details\":\"a:1:{i:0;a:10:{s:5:\\\"title\\\";s:3:\\\"ABC\\\";s:7:\\\"web_url\\\";s:12:\\\"facebook.com\\\";s:11:\\\"description\\\";s:14:\\\"<p>ABC<br><\\/p>\\\";s:5:\\\"price\\\";s:2:\\\"32\\\";s:2:\\\"da\\\";s:2:\\\"21\\\";s:2:\\\"fb\\\";s:3:\\\"323\\\";s:8:\\\"follower\\\";s:2:\\\"33\\\";s:10:\\\"blog_image\\\";N;s:7:\\\"blog_id\\\";s:1:\\\"6\\\";s:7:\\\"user_id\\\";s:2:\\\"14\\\";}}\",\"user_email\":\"ahsan.amin334@gmail.com\",\"updated_at\":\"2020-01-14 15:41:01\",\"created_at\":\"2020-01-14 15:41:01\",\"id\":188}}', 'App\\User', '2020-01-14 10:41:21', '2020-01-14 10:41:01', '2020-01-14 10:41:21'),
('5f0b36d9-49e3-4151-bb02-06905e785d13', 'App\\Notifications\\CreateOrders', 1, '{\"data\":{\"user_id\":14,\"user_meta\":\"a:10:{s:6:\\\"_token\\\";s:40:\\\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\\\";s:5:\\\"fname\\\";s:5:\\\"Yasin\\\";s:5:\\\"lname\\\";s:4:\\\"amin\\\";s:7:\\\"address\\\";s:7:\\\"testing\\\";s:4:\\\"city\\\";s:6:\\\"kaghan\\\";s:7:\\\"country\\\";s:14:\\\"\\u00c5land Islands\\\";s:9:\\\"post_code\\\";s:6:\\\"354354\\\";s:12:\\\"phone_number\\\";s:7:\\\"0423423\\\";s:5:\\\"email\\\";s:23:\\\"ahsan.amin334@gmail.com\\\";s:6:\\\"amount\\\";s:2:\\\"32\\\";}\",\"status\":\"pending\",\"total_amount\":32,\"order_details\":\"a:1:{i:0;a:10:{s:5:\\\"title\\\";s:3:\\\"ABC\\\";s:7:\\\"web_url\\\";s:12:\\\"facebook.com\\\";s:11:\\\"description\\\";s:14:\\\"<p>ABC<br><\\/p>\\\";s:5:\\\"price\\\";s:2:\\\"32\\\";s:2:\\\"da\\\";s:2:\\\"21\\\";s:2:\\\"fb\\\";s:3:\\\"323\\\";s:8:\\\"follower\\\";s:2:\\\"33\\\";s:10:\\\"blog_image\\\";N;s:7:\\\"blog_id\\\";s:1:\\\"6\\\";s:7:\\\"user_id\\\";s:2:\\\"14\\\";}}\",\"user_email\":\"ahsan.amin334@gmail.com\",\"updated_at\":\"2020-01-14 15:40:16\",\"created_at\":\"2020-01-14 15:40:16\",\"id\":184}}', 'App\\User', '2020-01-14 10:40:21', '2020-01-14 10:40:16', '2020-01-14 10:40:21'),
('69c88c96-65d2-433a-9960-b6aad92daf8b', 'App\\Notifications\\CreateOrders', 1, '{\"data\":{\"user_id\":14,\"user_meta\":\"a:10:{s:6:\\\"_token\\\";s:40:\\\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\\\";s:5:\\\"fname\\\";s:7:\\\"Tahseen\\\";s:5:\\\"lname\\\";s:4:\\\"amin\\\";s:7:\\\"address\\\";s:7:\\\"testing\\\";s:4:\\\"city\\\";s:6:\\\"kaghan\\\";s:7:\\\"country\\\";s:14:\\\"\\u00c5land Islands\\\";s:9:\\\"post_code\\\";s:6:\\\"354354\\\";s:12:\\\"phone_number\\\";s:7:\\\"0423423\\\";s:5:\\\"email\\\";s:23:\\\"ahsan.amin334@gmail.com\\\";s:6:\\\"amount\\\";s:2:\\\"32\\\";}\",\"status\":\"pending\",\"total_amount\":32,\"order_details\":\"a:1:{i:0;a:10:{s:5:\\\"title\\\";s:3:\\\"ABC\\\";s:7:\\\"web_url\\\";s:12:\\\"facebook.com\\\";s:11:\\\"description\\\";s:14:\\\"<p>ABC<br><\\/p>\\\";s:5:\\\"price\\\";s:2:\\\"32\\\";s:2:\\\"da\\\";s:2:\\\"21\\\";s:2:\\\"fb\\\";s:3:\\\"323\\\";s:8:\\\"follower\\\";s:2:\\\"33\\\";s:10:\\\"blog_image\\\";N;s:7:\\\"blog_id\\\";s:1:\\\"6\\\";s:7:\\\"user_id\\\";s:2:\\\"14\\\";}}\",\"user_email\":\"ahsan.amin334@gmail.com\",\"updated_at\":\"2020-01-14 14:38:02\",\"created_at\":\"2020-01-14 14:38:02\",\"id\":181}}', 'App\\User', '2020-01-14 10:37:04', '2020-01-14 09:38:03', '2020-01-14 10:37:04'),
('88113e81-30ec-4398-9865-350082e4928a', 'App\\Notifications\\CreateOrders', 1, '{\"data\":{\"user_id\":14,\"user_meta\":\"a:10:{s:6:\\\"_token\\\";s:40:\\\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\\\";s:5:\\\"fname\\\";s:5:\\\"Yasin\\\";s:5:\\\"lname\\\";s:4:\\\"amin\\\";s:7:\\\"address\\\";s:7:\\\"testing\\\";s:4:\\\"city\\\";s:6:\\\"kaghan\\\";s:7:\\\"country\\\";s:14:\\\"\\u00c5land Islands\\\";s:9:\\\"post_code\\\";s:6:\\\"354354\\\";s:12:\\\"phone_number\\\";s:7:\\\"0423423\\\";s:5:\\\"email\\\";s:23:\\\"ahsan.amin334@gmail.com\\\";s:6:\\\"amount\\\";s:2:\\\"32\\\";}\",\"status\":\"pending\",\"total_amount\":32,\"order_details\":\"a:1:{i:0;a:10:{s:5:\\\"title\\\";s:3:\\\"ABC\\\";s:7:\\\"web_url\\\";s:12:\\\"facebook.com\\\";s:11:\\\"description\\\";s:14:\\\"<p>ABC<br><\\/p>\\\";s:5:\\\"price\\\";s:2:\\\"32\\\";s:2:\\\"da\\\";s:2:\\\"21\\\";s:2:\\\"fb\\\";s:3:\\\"323\\\";s:8:\\\"follower\\\";s:2:\\\"33\\\";s:10:\\\"blog_image\\\";N;s:7:\\\"blog_id\\\";s:1:\\\"6\\\";s:7:\\\"user_id\\\";s:2:\\\"14\\\";}}\",\"user_email\":\"ahsan.amin334@gmail.com\",\"updated_at\":\"2020-01-14 15:42:45\",\"created_at\":\"2020-01-14 15:42:45\",\"id\":190}}', 'App\\User', '2020-01-14 10:42:54', '2020-01-14 10:42:45', '2020-01-14 10:42:54'),
('953dbd52-3975-4e29-8cdb-278dd6cb85d5', 'App\\Notifications\\CreateOrders', 1, '{\"data\":{\"user_id\":14,\"user_meta\":\"a:10:{s:6:\\\"_token\\\";s:40:\\\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\\\";s:5:\\\"fname\\\";s:7:\\\"Tahseen\\\";s:5:\\\"lname\\\";s:4:\\\"amin\\\";s:7:\\\"address\\\";s:7:\\\"testing\\\";s:4:\\\"city\\\";s:6:\\\"kaghan\\\";s:7:\\\"country\\\";s:14:\\\"\\u00c5land Islands\\\";s:9:\\\"post_code\\\";s:6:\\\"354354\\\";s:12:\\\"phone_number\\\";s:7:\\\"0423423\\\";s:5:\\\"email\\\";s:23:\\\"ahsan.amin334@gmail.com\\\";s:6:\\\"amount\\\";s:2:\\\"32\\\";}\",\"status\":\"pending\",\"total_amount\":32,\"order_details\":\"a:1:{i:0;a:10:{s:5:\\\"title\\\";s:3:\\\"ABC\\\";s:7:\\\"web_url\\\";s:12:\\\"facebook.com\\\";s:11:\\\"description\\\";s:14:\\\"<p>ABC<br><\\/p>\\\";s:5:\\\"price\\\";s:2:\\\"32\\\";s:2:\\\"da\\\";s:2:\\\"21\\\";s:2:\\\"fb\\\";s:3:\\\"323\\\";s:8:\\\"follower\\\";s:2:\\\"33\\\";s:10:\\\"blog_image\\\";N;s:7:\\\"blog_id\\\";s:1:\\\"6\\\";s:7:\\\"user_id\\\";s:2:\\\"14\\\";}}\",\"user_email\":\"ahsan.amin334@gmail.com\",\"updated_at\":\"2020-01-14 15:34:37\",\"created_at\":\"2020-01-14 15:34:37\",\"id\":182}}', 'App\\User', '2020-01-14 10:38:30', '2020-01-14 10:34:37', '2020-01-14 10:38:30'),
('ac9a4e6d-1e1d-4fca-b9f0-62328b396a15', 'App\\Notifications\\CreateOrders', 1, '{\"data\":{\"user_id\":14,\"user_meta\":\"a:10:{s:6:\\\"_token\\\";s:40:\\\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\\\";s:5:\\\"fname\\\";s:5:\\\"Ahsan\\\";s:5:\\\"lname\\\";s:4:\\\"amin\\\";s:7:\\\"address\\\";s:7:\\\"testing\\\";s:4:\\\"city\\\";s:6:\\\"kaghan\\\";s:7:\\\"country\\\";s:14:\\\"\\u00c5land Islands\\\";s:9:\\\"post_code\\\";s:6:\\\"354354\\\";s:12:\\\"phone_number\\\";s:7:\\\"0423423\\\";s:5:\\\"email\\\";s:23:\\\"ahsan.amin334@gmail.com\\\";s:6:\\\"amount\\\";s:2:\\\"32\\\";}\",\"status\":\"pending\",\"total_amount\":32,\"order_details\":\"a:1:{i:0;a:10:{s:5:\\\"title\\\";s:3:\\\"ABC\\\";s:7:\\\"web_url\\\";s:12:\\\"facebook.com\\\";s:11:\\\"description\\\";s:14:\\\"<p>ABC<br><\\/p>\\\";s:5:\\\"price\\\";s:2:\\\"32\\\";s:2:\\\"da\\\";s:2:\\\"21\\\";s:2:\\\"fb\\\";s:3:\\\"323\\\";s:8:\\\"follower\\\";s:2:\\\"33\\\";s:10:\\\"blog_image\\\";N;s:7:\\\"blog_id\\\";s:1:\\\"6\\\";s:7:\\\"user_id\\\";s:2:\\\"14\\\";}}\",\"user_email\":\"ahsan.amin334@gmail.com\",\"updated_at\":\"2020-01-14 14:05:18\",\"created_at\":\"2020-01-14 14:05:18\",\"id\":179}}', 'App\\User', '2020-01-14 10:38:33', '2020-01-14 09:05:18', '2020-01-14 10:38:33'),
('c1ea41f0-dd94-43cf-80c2-6001e201ba0e', 'App\\Notifications\\CreateOrders', 1, '{\"data\":{\"user_id\":14,\"user_meta\":\"a:10:{s:6:\\\"_token\\\";s:40:\\\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\\\";s:5:\\\"fname\\\";s:7:\\\"Tahseen\\\";s:5:\\\"lname\\\";s:4:\\\"amin\\\";s:7:\\\"address\\\";s:7:\\\"testing\\\";s:4:\\\"city\\\";s:6:\\\"kaghan\\\";s:7:\\\"country\\\";s:14:\\\"\\u00c5land Islands\\\";s:9:\\\"post_code\\\";s:6:\\\"354354\\\";s:12:\\\"phone_number\\\";s:7:\\\"0423423\\\";s:5:\\\"email\\\";s:23:\\\"ahsan.amin334@gmail.com\\\";s:6:\\\"amount\\\";s:2:\\\"32\\\";}\",\"status\":\"pending\",\"total_amount\":32,\"order_details\":\"a:1:{i:0;a:10:{s:5:\\\"title\\\";s:3:\\\"ABC\\\";s:7:\\\"web_url\\\";s:12:\\\"facebook.com\\\";s:11:\\\"description\\\";s:14:\\\"<p>ABC<br><\\/p>\\\";s:5:\\\"price\\\";s:2:\\\"32\\\";s:2:\\\"da\\\";s:2:\\\"21\\\";s:2:\\\"fb\\\";s:3:\\\"323\\\";s:8:\\\"follower\\\";s:2:\\\"33\\\";s:10:\\\"blog_image\\\";N;s:7:\\\"blog_id\\\";s:1:\\\"6\\\";s:7:\\\"user_id\\\";s:2:\\\"14\\\";}}\",\"user_email\":\"ahsan.amin334@gmail.com\",\"updated_at\":\"2020-01-14 15:38:52\",\"created_at\":\"2020-01-14 15:38:52\",\"id\":183}}', 'App\\User', '2020-01-14 10:38:59', '2020-01-14 10:38:52', '2020-01-14 10:38:59'),
('cc59150a-e924-4555-98da-9a35e3f4fb6e', 'App\\Notifications\\CreateOrders', 1, '{\"data\":{\"user_id\":14,\"user_meta\":\"a:10:{s:6:\\\"_token\\\";s:40:\\\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\\\";s:5:\\\"fname\\\";N;s:5:\\\"lname\\\";N;s:7:\\\"address\\\";N;s:4:\\\"city\\\";N;s:7:\\\"country\\\";N;s:9:\\\"post_code\\\";N;s:12:\\\"phone_number\\\";N;s:5:\\\"email\\\";s:23:\\\"ahsan.amin334@gmail.com\\\";s:6:\\\"amount\\\";s:2:\\\"32\\\";}\",\"status\":\"pending\",\"total_amount\":32,\"order_details\":\"a:1:{i:0;a:10:{s:5:\\\"title\\\";s:3:\\\"ABC\\\";s:7:\\\"web_url\\\";s:12:\\\"facebook.com\\\";s:11:\\\"description\\\";s:14:\\\"<p>ABC<br><\\/p>\\\";s:5:\\\"price\\\";s:2:\\\"32\\\";s:2:\\\"da\\\";s:2:\\\"21\\\";s:2:\\\"fb\\\";s:3:\\\"323\\\";s:8:\\\"follower\\\";s:2:\\\"33\\\";s:10:\\\"blog_image\\\";N;s:7:\\\"blog_id\\\";s:1:\\\"6\\\";s:7:\\\"user_id\\\";s:2:\\\"14\\\";}}\",\"user_email\":\"ahsan.amin334@gmail.com\",\"updated_at\":\"2020-01-14 13:06:10\",\"created_at\":\"2020-01-14 13:06:10\",\"id\":177}}', 'App\\User', '2020-01-14 10:38:28', '2020-01-14 08:06:10', '2020-01-14 10:38:28'),
('d864156f-77fd-4a16-a336-9f5c44bdf449', 'App\\Notifications\\CreateOrders', 1, '{\"data\":{\"user_id\":14,\"user_meta\":\"a:10:{s:6:\\\"_token\\\";s:40:\\\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\\\";s:5:\\\"fname\\\";s:5:\\\"Yasin\\\";s:5:\\\"lname\\\";s:4:\\\"amin\\\";s:7:\\\"address\\\";s:7:\\\"testing\\\";s:4:\\\"city\\\";s:6:\\\"kaghan\\\";s:7:\\\"country\\\";s:14:\\\"\\u00c5land Islands\\\";s:9:\\\"post_code\\\";s:6:\\\"354354\\\";s:12:\\\"phone_number\\\";s:7:\\\"0423423\\\";s:5:\\\"email\\\";s:23:\\\"ahsan.amin334@gmail.com\\\";s:6:\\\"amount\\\";s:2:\\\"32\\\";}\",\"status\":\"pending\",\"total_amount\":32,\"order_details\":\"a:1:{i:0;a:10:{s:5:\\\"title\\\";s:3:\\\"ABC\\\";s:7:\\\"web_url\\\";s:12:\\\"facebook.com\\\";s:11:\\\"description\\\";s:14:\\\"<p>ABC<br><\\/p>\\\";s:5:\\\"price\\\";s:2:\\\"32\\\";s:2:\\\"da\\\";s:2:\\\"21\\\";s:2:\\\"fb\\\";s:3:\\\"323\\\";s:8:\\\"follower\\\";s:2:\\\"33\\\";s:10:\\\"blog_image\\\";N;s:7:\\\"blog_id\\\";s:1:\\\"6\\\";s:7:\\\"user_id\\\";s:2:\\\"14\\\";}}\",\"user_email\":\"ahsan.amin334@gmail.com\",\"updated_at\":\"2020-01-14 15:40:27\",\"created_at\":\"2020-01-14 15:40:27\",\"id\":185}}', 'App\\User', '2020-01-14 10:40:50', '2020-01-14 10:40:27', '2020-01-14 10:40:50'),
('efe5fac8-18a9-454d-ad76-9252584aeaf5', 'App\\Notifications\\CreateOrders', 1, '{\"data\":{\"user_id\":14,\"user_meta\":\"a:10:{s:6:\\\"_token\\\";s:40:\\\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\\\";s:5:\\\"fname\\\";s:5:\\\"Yasin\\\";s:5:\\\"lname\\\";s:4:\\\"amin\\\";s:7:\\\"address\\\";s:7:\\\"testing\\\";s:4:\\\"city\\\";s:6:\\\"kaghan\\\";s:7:\\\"country\\\";s:14:\\\"\\u00c5land Islands\\\";s:9:\\\"post_code\\\";s:6:\\\"354354\\\";s:12:\\\"phone_number\\\";s:7:\\\"0423423\\\";s:5:\\\"email\\\";s:23:\\\"ahsan.amin334@gmail.com\\\";s:6:\\\"amount\\\";s:2:\\\"32\\\";}\",\"status\":\"pending\",\"total_amount\":32,\"order_details\":\"a:1:{i:0;a:10:{s:5:\\\"title\\\";s:3:\\\"ABC\\\";s:7:\\\"web_url\\\";s:12:\\\"facebook.com\\\";s:11:\\\"description\\\";s:14:\\\"<p>ABC<br><\\/p>\\\";s:5:\\\"price\\\";s:2:\\\"32\\\";s:2:\\\"da\\\";s:2:\\\"21\\\";s:2:\\\"fb\\\";s:3:\\\"323\\\";s:8:\\\"follower\\\";s:2:\\\"33\\\";s:10:\\\"blog_image\\\";N;s:7:\\\"blog_id\\\";s:1:\\\"6\\\";s:7:\\\"user_id\\\";s:2:\\\"14\\\";}}\",\"user_email\":\"ahsan.amin334@gmail.com\",\"updated_at\":\"2020-01-14 15:41:53\",\"created_at\":\"2020-01-14 15:41:53\",\"id\":189}}', 'App\\User', '2020-01-14 10:42:03', '2020-01-14 10:41:53', '2020-01-14 10:42:03'),
('fd9d1537-6328-42c5-8fb7-e307e8e4e41b', 'App\\Notifications\\CreateOrders', 1, '{\"data\":{\"user_id\":14,\"user_meta\":\"a:10:{s:6:\\\"_token\\\";s:40:\\\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\\\";s:5:\\\"fname\\\";s:5:\\\"Ahsan\\\";s:5:\\\"lname\\\";s:4:\\\"amin\\\";s:7:\\\"address\\\";s:7:\\\"testing\\\";s:4:\\\"city\\\";s:6:\\\"kaghan\\\";s:7:\\\"country\\\";s:14:\\\"\\u00c5land Islands\\\";s:9:\\\"post_code\\\";s:6:\\\"354354\\\";s:12:\\\"phone_number\\\";s:7:\\\"0423423\\\";s:5:\\\"email\\\";s:23:\\\"ahsan.amin334@gmail.com\\\";s:6:\\\"amount\\\";s:2:\\\"32\\\";}\",\"status\":\"pending\",\"total_amount\":32,\"order_details\":\"a:1:{i:0;a:10:{s:5:\\\"title\\\";s:3:\\\"ABC\\\";s:7:\\\"web_url\\\";s:12:\\\"facebook.com\\\";s:11:\\\"description\\\";s:14:\\\"<p>ABC<br><\\/p>\\\";s:5:\\\"price\\\";s:2:\\\"32\\\";s:2:\\\"da\\\";s:2:\\\"21\\\";s:2:\\\"fb\\\";s:3:\\\"323\\\";s:8:\\\"follower\\\";s:2:\\\"33\\\";s:10:\\\"blog_image\\\";N;s:7:\\\"blog_id\\\";s:1:\\\"6\\\";s:7:\\\"user_id\\\";s:2:\\\"14\\\";}}\",\"user_email\":\"ahsan.amin334@gmail.com\",\"updated_at\":\"2020-01-14 14:34:59\",\"created_at\":\"2020-01-14 14:34:59\",\"id\":180}}', 'App\\User', '2020-01-14 10:38:31', '2020-01-14 09:34:59', '2020-01-14 10:38:31');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_details` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_meta` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_amount` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paypal_limit_response` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_all_response` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `user_email`, `order_details`, `user_meta`, `created_at`, `updated_at`, `total_amount`, `status`, `paypal_limit_response`, `paypal_all_response`) VALUES
(177, 14, 'ahsan.amin334@gmail.com', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}', 'a:10:{s:6:\"_token\";s:40:\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\";s:5:\"fname\";N;s:5:\"lname\";N;s:7:\"address\";N;s:4:\"city\";N;s:7:\"country\";N;s:9:\"post_code\";N;s:12:\"phone_number\";N;s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:6:\"amount\";s:2:\"32\";}', '2020-01-14 08:06:10', '2020-01-14 08:06:10', 32, 'pending', NULL, NULL),
(178, 14, 'ahsan.amin334@gmail.com', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}', 'a:10:{s:6:\"_token\";s:40:\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\";s:5:\"fname\";N;s:5:\"lname\";N;s:7:\"address\";N;s:4:\"city\";N;s:7:\"country\";N;s:9:\"post_code\";N;s:12:\"phone_number\";N;s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:6:\"amount\";s:2:\"32\";}', '2020-01-14 08:13:48', '2020-01-14 08:13:48', 32, 'pending', NULL, NULL),
(179, 14, 'ahsan.amin334@gmail.com', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}', 'a:10:{s:6:\"_token\";s:40:\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\";s:5:\"fname\";s:5:\"Ahsan\";s:5:\"lname\";s:4:\"amin\";s:7:\"address\";s:7:\"testing\";s:4:\"city\";s:6:\"kaghan\";s:7:\"country\";s:14:\"Åland Islands\";s:9:\"post_code\";s:6:\"354354\";s:12:\"phone_number\";s:7:\"0423423\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:6:\"amount\";s:2:\"32\";}', '2020-01-14 09:05:18', '2020-01-14 09:05:18', 32, 'pending', NULL, NULL),
(180, 14, 'ahsan.amin334@gmail.com', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}', 'a:10:{s:6:\"_token\";s:40:\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\";s:5:\"fname\";s:5:\"Ahsan\";s:5:\"lname\";s:4:\"amin\";s:7:\"address\";s:7:\"testing\";s:4:\"city\";s:6:\"kaghan\";s:7:\"country\";s:14:\"Åland Islands\";s:9:\"post_code\";s:6:\"354354\";s:12:\"phone_number\";s:7:\"0423423\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:6:\"amount\";s:2:\"32\";}', '2020-01-14 09:34:59', '2020-01-14 09:34:59', 32, 'pending', NULL, NULL),
(181, 14, 'ahsan.amin334@gmail.com', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}', 'a:10:{s:6:\"_token\";s:40:\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\";s:5:\"fname\";s:7:\"Tahseen\";s:5:\"lname\";s:4:\"amin\";s:7:\"address\";s:7:\"testing\";s:4:\"city\";s:6:\"kaghan\";s:7:\"country\";s:14:\"Åland Islands\";s:9:\"post_code\";s:6:\"354354\";s:12:\"phone_number\";s:7:\"0423423\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:6:\"amount\";s:2:\"32\";}', '2020-01-14 09:38:02', '2020-01-14 09:38:02', 32, 'pending', NULL, NULL),
(182, 14, 'ahsan.amin334@gmail.com', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}', 'a:10:{s:6:\"_token\";s:40:\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\";s:5:\"fname\";s:7:\"Tahseen\";s:5:\"lname\";s:4:\"amin\";s:7:\"address\";s:7:\"testing\";s:4:\"city\";s:6:\"kaghan\";s:7:\"country\";s:14:\"Åland Islands\";s:9:\"post_code\";s:6:\"354354\";s:12:\"phone_number\";s:7:\"0423423\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:6:\"amount\";s:2:\"32\";}', '2020-01-14 10:34:37', '2020-01-14 10:34:37', 32, 'pending', NULL, NULL),
(183, 14, 'ahsan.amin334@gmail.com', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}', 'a:10:{s:6:\"_token\";s:40:\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\";s:5:\"fname\";s:7:\"Tahseen\";s:5:\"lname\";s:4:\"amin\";s:7:\"address\";s:7:\"testing\";s:4:\"city\";s:6:\"kaghan\";s:7:\"country\";s:14:\"Åland Islands\";s:9:\"post_code\";s:6:\"354354\";s:12:\"phone_number\";s:7:\"0423423\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:6:\"amount\";s:2:\"32\";}', '2020-01-14 10:38:52', '2020-01-14 10:38:52', 32, 'pending', NULL, NULL),
(184, 14, 'ahsan.amin334@gmail.com', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}', 'a:10:{s:6:\"_token\";s:40:\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\";s:5:\"fname\";s:5:\"Yasin\";s:5:\"lname\";s:4:\"amin\";s:7:\"address\";s:7:\"testing\";s:4:\"city\";s:6:\"kaghan\";s:7:\"country\";s:14:\"Åland Islands\";s:9:\"post_code\";s:6:\"354354\";s:12:\"phone_number\";s:7:\"0423423\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:6:\"amount\";s:2:\"32\";}', '2020-01-14 10:40:16', '2020-01-14 10:40:16', 32, 'pending', NULL, NULL),
(185, 14, 'ahsan.amin334@gmail.com', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}', 'a:10:{s:6:\"_token\";s:40:\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\";s:5:\"fname\";s:5:\"Yasin\";s:5:\"lname\";s:4:\"amin\";s:7:\"address\";s:7:\"testing\";s:4:\"city\";s:6:\"kaghan\";s:7:\"country\";s:14:\"Åland Islands\";s:9:\"post_code\";s:6:\"354354\";s:12:\"phone_number\";s:7:\"0423423\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:6:\"amount\";s:2:\"32\";}', '2020-01-14 10:40:27', '2020-01-14 10:40:27', 32, 'pending', NULL, NULL),
(186, 14, 'ahsan.amin334@gmail.com', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}', 'a:10:{s:6:\"_token\";s:40:\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\";s:5:\"fname\";s:5:\"Yasin\";s:5:\"lname\";s:4:\"amin\";s:7:\"address\";s:7:\"testing\";s:4:\"city\";s:6:\"kaghan\";s:7:\"country\";s:14:\"Åland Islands\";s:9:\"post_code\";s:6:\"354354\";s:12:\"phone_number\";s:7:\"0423423\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:6:\"amount\";s:2:\"32\";}', '2020-01-14 10:40:59', '2020-01-14 10:40:59', 32, 'pending', NULL, NULL),
(187, 14, 'ahsan.amin334@gmail.com', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}', 'a:10:{s:6:\"_token\";s:40:\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\";s:5:\"fname\";s:5:\"Yasin\";s:5:\"lname\";s:4:\"amin\";s:7:\"address\";s:7:\"testing\";s:4:\"city\";s:6:\"kaghan\";s:7:\"country\";s:14:\"Åland Islands\";s:9:\"post_code\";s:6:\"354354\";s:12:\"phone_number\";s:7:\"0423423\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:6:\"amount\";s:2:\"32\";}', '2020-01-14 10:41:00', '2020-01-14 10:41:00', 32, 'pending', NULL, NULL),
(188, 14, 'ahsan.amin334@gmail.com', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}', 'a:10:{s:6:\"_token\";s:40:\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\";s:5:\"fname\";s:5:\"Yasin\";s:5:\"lname\";s:4:\"amin\";s:7:\"address\";s:7:\"testing\";s:4:\"city\";s:6:\"kaghan\";s:7:\"country\";s:14:\"Åland Islands\";s:9:\"post_code\";s:6:\"354354\";s:12:\"phone_number\";s:7:\"0423423\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:6:\"amount\";s:2:\"32\";}', '2020-01-14 10:41:01', '2020-01-14 10:41:01', 32, 'pending', NULL, NULL),
(189, 14, 'ahsan.amin334@gmail.com', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}', 'a:10:{s:6:\"_token\";s:40:\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\";s:5:\"fname\";s:5:\"Yasin\";s:5:\"lname\";s:4:\"amin\";s:7:\"address\";s:7:\"testing\";s:4:\"city\";s:6:\"kaghan\";s:7:\"country\";s:14:\"Åland Islands\";s:9:\"post_code\";s:6:\"354354\";s:12:\"phone_number\";s:7:\"0423423\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:6:\"amount\";s:2:\"32\";}', '2020-01-14 10:41:53', '2020-01-14 10:41:53', 32, 'pending', NULL, NULL),
(190, 14, 'ahsan.amin334@gmail.com', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}', 'a:10:{s:6:\"_token\";s:40:\"mRmbeQ4kio461LtxOQNR9GSCVf23TxaFQxEbAakB\";s:5:\"fname\";s:5:\"Yasin\";s:5:\"lname\";s:4:\"amin\";s:7:\"address\";s:7:\"testing\";s:4:\"city\";s:6:\"kaghan\";s:7:\"country\";s:14:\"Åland Islands\";s:9:\"post_code\";s:6:\"354354\";s:12:\"phone_number\";s:7:\"0423423\";s:5:\"email\";s:23:\"ahsan.amin334@gmail.com\";s:6:\"amount\";s:2:\"32\";}', '2020-01-14 10:42:45', '2020-01-14 10:42:45', 32, 'pending', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_blogs`
--

CREATE TABLE `order_blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `blog_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_log`
--

CREATE TABLE `order_log` (
  `id` int(11) NOT NULL,
  `user_id` int(50) NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_log`
--

INSERT INTO `order_log` (`id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'add-to-cart-details', 'a:10:{s:6:\"_token\";s:40:\"UUkHU8BLLKMCKZGxfAm6HsjS18WTt1Amkv2dXR5O\";s:5:\"title\";s:6:\"title1\";s:3:\"url\";s:66:\"http://localhost/guest-posting-portal-new/public/blogs/add_to_cart\";s:11:\"description\";s:26:\"<p>this is description</p>\";s:5:\"price\";s:2:\"46\";s:2:\"da\";s:3:\"102\";s:2:\"fb\";s:2:\"10\";s:8:\"follower\";s:2:\"10\";s:7:\"user_id\";s:1:\"1\";s:7:\"blog_id\";s:1:\"4\";}'),
(38, 14, 'add-to-cart-details', 'a:1:{i:0;a:10:{s:5:\"title\";s:3:\"ABC\";s:7:\"web_url\";s:12:\"facebook.com\";s:11:\"description\";s:14:\"<p>ABC<br></p>\";s:5:\"price\";s:2:\"32\";s:2:\"da\";s:2:\"21\";s:2:\"fb\";s:3:\"323\";s:8:\"follower\";s:2:\"33\";s:10:\"blog_image\";N;s:7:\"blog_id\";s:1:\"6\";s:7:\"user_id\";s:2:\"14\";}}');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'add user', 'web', '2019-07-26 06:01:49', '2019-07-26 06:01:49'),
(2, 'Administer roles & permissions', 'web', '2019-07-29 09:51:55', '2019-07-29 09:51:55');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2019-07-26 06:03:22', '2019-07-26 06:03:22');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT 0,
  `credit` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT 'no',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `verified`, `credit`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '2020-01-08 09:35:50', '$2y$10$CIX0RD3NNo75/aBkDlNaFedNvEuLuDXpLE7MXZbGy.Ga00PEWX/H6', 1, 'yes', NULL, '2019-07-22 09:25:57', '2019-07-26 06:17:40'),
(2, 'asad', 'asad@example.com', '2020-01-08 09:35:56', '$2y$10$nbq27n9AtAs0Tsv3SwneNOAb/gJz9act826EyrulLaX3GOR.bUGJy', 1, 'yes', NULL, '2019-07-29 04:45:29', '2019-07-29 04:45:29'),
(6, 'Tahseen', 'tahseen@gmail.com', '2020-01-08 09:17:13', '$2y$10$GnkG5rrVuwdNLxWbWbunDuavYt5qK32Dcf3.0mbjaSFHj274ah0PK', 1, 'no', NULL, '2020-01-08 02:45:51', '2020-01-08 02:49:11'),
(7, 'readonly', 'readonly@gmail.com', '2020-01-08 09:17:14', '$2y$10$H11jwi.AnAC19NhdZYA6U.EKshdmxiu0zYJjN6pbSOPMJ.Ej74kwC', 1, 'no', NULL, '2020-01-08 02:50:53', '2020-01-08 02:52:08'),
(8, 'ahsanreadonly@gmail.com', 'ahsanreadonly@gmail.com', '2020-01-08 09:17:16', '$2y$10$Khn8nEq.aAOFx1sgKotsFusIubZc38zXcm4PyRCu9fj2g611qDLRy', 1, 'no', NULL, '2020-01-08 02:53:07', '2020-01-08 02:53:28'),
(9, 'yasin', 'yasim12@gmail.com', '2020-01-08 09:17:18', '$2y$10$oNaljQqSSwXMYUNSGsDYCuDIKm3gbwhskVeQJLXuXr8S0AXrLKI9q', 1, 'no', NULL, '2020-01-08 02:59:44', '2020-01-08 03:00:11'),
(10, 'Tahseen', 'tahseen21@gmail.com', '2020-01-08 09:17:21', '$2y$10$AIJ4RE3RSz140CpQjD9RhuSQSMmIRE8j8dX1124aJ29ErmkrH.IDO', 0, 'no', NULL, '2020-01-08 03:02:15', '2020-01-08 03:02:15'),
(11, 'Tahseen', 'tahseen22@gmail.com', '2020-01-08 09:17:23', '$2y$10$alGpmnDxRzl6h7/qDPBDHe2u/LbAlYEffh5Ji0J6nlc.OsR8xXf2e', 0, 'no', NULL, '2020-01-08 03:02:36', '2020-01-08 03:02:36'),
(12, 'Tahseen213', 'Tahseen213@gmail.com', '2020-01-08 09:17:25', '$2y$10$y8EqCE.X541JLHqLnVQLtOumNsKYd3tHD.vB5vAGCVCKJygz7JQ4u', 1, 'no', NULL, '2020-01-08 03:04:35', '2020-01-08 03:05:20'),
(13, 'ahsan', 'ahsan234@gmail.com', '2020-01-08 09:17:27', '$2y$10$TTbST6643fgVotzS2H63vOjNcW9taPiHyquy8iqALn.I.xeWh5JMG', 1, 'no', NULL, '2020-01-08 03:20:32', '2020-01-08 03:20:48'),
(14, 'ahsan', 'ahsan.amin334@gmail.com', '2020-01-08 09:37:55', '$2y$10$qWi8abxjrw0bMChQRhb0u./vy1HFeCH1rhzx/C2MryM9PrAFJkUvi', 1, 'yes', NULL, '2020-01-08 04:36:50', '2020-01-08 04:37:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_blogs`
--

CREATE TABLE `user_blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `blogs_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_metas`
--

CREATE TABLE `user_metas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `verify_users`
--

CREATE TABLE `verify_users` (
  `id` int(11) NOT NULL,
  `user_id` int(200) NOT NULL,
  `token` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `verify_users`
--

INSERT INTO `verify_users` (`id`, `user_id`, `token`) VALUES
(3, 6, 'FJrghev8x8fCUPcCPmMljXaFk0IR0DhZVBanP5A4'),
(4, 7, 'Eizl3sYWl4AVozhrjpWVtXTrFE8d9cFFlI5xrhAi'),
(5, 8, 'NWp7nAa0iavWZTyJEssJfWagwyyX1OLsjdntfYud'),
(6, 9, 'wZOKXE9KrqQZ1Myly0x4I7RXZ3bQqN5A3isT0Uvg'),
(7, 10, '6eOLXLSLmrrnOxHb2IFN0yhK1gZQSHAC5gf7FAtU'),
(8, 11, 'NRzPRklB40qK4twEr7hgmLrQ5vdHu2SpWkuErNkc'),
(9, 12, 'ixlFfWyRNj0QEYISn7puj8YWaOkx5MBAA1HULLZN'),
(10, 13, 'LMDhzpN2XKL5PzpuCPUs324EthqbnGSFnVaHAjoN'),
(11, 14, 'jrhZdO9umQqsmKGqaCiCzDbZEQKbWqPDCNQ6MCsw');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_industries`
--
ALTER TABLE `blog_industries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_industries_blog_id_foreign` (`blog_id`),
  ADD KEY `blog_industries_industry_id_foreign` (`industry_id`);

--
-- Indexes for table `blog_keywords`
--
ALTER TABLE `blog_keywords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_keywords_blog_id_foreign` (`blog_id`),
  ADD KEY `blog_keywords_keyword_id_foreign` (`keyword_id`);

--
-- Indexes for table `blog_metas`
--
ALTER TABLE `blog_metas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_metas_blog_id_foreign` (`blog_id`);

--
-- Indexes for table `credit_histories`
--
ALTER TABLE `credit_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `credit_histories_user_id_foreign` (`user_id`);

--
-- Indexes for table `industries`
--
ALTER TABLE `industries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keywords`
--
ALTER TABLE `keywords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `order_log`
--
ALTER TABLE `order_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verify_users`
--
ALTER TABLE `verify_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT for table `order_log`
--
ALTER TABLE `order_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `verify_users`
--
ALTER TABLE `verify_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
