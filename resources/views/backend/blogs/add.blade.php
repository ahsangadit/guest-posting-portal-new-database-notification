@extends('layouts.backend.app')

@section('section')
    <div class="content-wrapper">

    @include('layouts.backend.alert')

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Blogs
                <small>Add Blog</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('Home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{route('blogs.index')}}">Blogs</a></li>
                <li class="active">Add Blog</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">

                            <form method="post" action="{{ route('blogs.store') }}" enctype='multipart/form-data'
                                  accept-charset="UTF-8">
                                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('link', 'Blog Link') }}
                                        {{ Form::text('link', '', array('class' => 'form-control @error("link") is-invalid @enderror')) }}

                                        @error('link')
                                            <span class="invalid-feedback" role="alert" style="color: red;">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                    </div>

                                    <div class="form-group">
                                        {{ Form::label('name', 'Blog Title') }}
                                        {{ Form::text('title', '', array('class' => 'form-control @error("title") is-invalid @enderror')) }}

                                        @error('title')
                                            <span class="invalid-feedback" role="alert" style="color: red;">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        {{ Form::label('name', 'Price') }}
                                        {{ Form::number('price', '', array('class' => 'form-control @error("price") is-invalid @enderror')) }}

                                        @error('price')
                                            <span class="invalid-feedback" role="alert" style="color: red;">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                    </div>

                                    <div class="form-group">
                                        {{ Form::label('name', 'Domain Authority') }}
                                        {{ Form::number('da', '', array('class' => 'form-control @error("da") is-invalid @enderror')) }}

                                        @error('da')
                                            <span class="invalid-feedback" role="alert" style="color: red;">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                    </div>

                                    <div class="form-group">
                                        {{ Form::label('name', 'PA') }}
                                        {{ Form::number('fb_likes', '', array('class' => 'form-control @error("fb_likes") is-invalid @enderror')) }}

                                        @error('fb_likes')
                                            <span class="invalid-feedback" role="alert" style="color: red;">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                    </div>

                                    <div class="form-group">
                                        {{ Form::label('name', 'Ahrefs Organic Traffic') }}
                                        {{ Form::number('follower', '', array('class' => 'form-control @error("follower") is-invalid @enderror')) }}

                                        @error('follower')
                                            <span class="invalid-feedback" role="alert" style="color: red;">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                    </div>

                                    <div class="form-group">
                                        {{ Form::label('name', 'Industries') }}
                                        {{ Form::select('industries',$industries,null,array('multiple'=>'multiple','name'=>'industry[]','class' => 'form-control select2 @error("industries") is-invalid @enderror','data-placeholder'=>'Select a Industry'))}}

                                        @error('industries')
                                            <span class="invalid-feedback" role="alert" style="color: red;">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('name', 'Keywords') }}
                                        {{Form::select('keywords',$keywords,null,array('multiple'=>'multiple','name'=>'keyword[]','class' => 'form-control select2 @error("keywords") is-invalid @enderror','data-placeholder'=>'Select a keyword'))}}

                                        @error('keywords')
                                            <span class="invalid-feedback" role="alert" style="color: red;">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('name', 'Description') }}
                                        {{ Form::textarea('description', '', array('class' => 'form-control textarea @error("description") is-invalid @enderror')) }}

                                        @error('description')
                                            <span class="invalid-feedback" role="alert" style="color: red;">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{ Form::label('name', 'Upload Image') }}
                                                {{ Form::file('image' , array('class' => 'form-control @error("image") is-invalid @enderror'))}}
                                                <p class="help-block">Upload blog picture here.</p>

                                                @error('image')
                                                    <span class="invalid-feedback" role="alert" style="color: red;">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{ Form::label('name', 'Dropped') }}
                                                {{ Form::radio('dropped', 'true' , true,array('class' => 'minimal-red')) }}
                                                {{ Form::radio('dropped', 'false' , true,array('class' => 'minimal-red')) }}
                                            </div>
                                        </div>
                                    </div>

                                    {{ Form::submit('Add Blog', array('class' => 'btn bg-navy')) }}
                                </div>

                                {{-- {{ Form::close() }} --}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    {{--@endsection--}}
@endsection

@section('script')
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            })
            $('.select2').select2();
            // $('.textarea').wysihtml5();
        })
    </script>

@endsection
