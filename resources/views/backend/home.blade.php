@extends('layouts.backend.app')

@section('section')

    <?php

    use App\orders;
    use App\keyword;
    use App\Industry;
    use App\blog;


    $user       = Auth::user();
    $orders     = "";
    $keywords   = "";
    $industries = "";
    $blogs      = "";

    if($user->hasRole(1)){
        $orders = orders::latest()->get();
    }
    else{
        $current_user_id = Auth::user()->id;
        $orders = orders::where('user_id',$current_user_id)->latest()->get();
    }

    $blogs      = blog::all();
    $keywords   = keyword::all();
    $industries = Industry::all();


    ?>


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('Home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                @hasrole('admin')
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>{{count($orders)}}</h3>

                                <p>Total Orders</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            <a href="{{route('orders.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>{{count($keywords)}} <sup style="font-size: 20px"></sup></h3>

                                <p>Keywords</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-font"></i>
                            </div>
                            <a href="{{route('keywords.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>{{count($industries)}}</h3>

                                <p>Industries</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-industry"></i>
                            </div>
                            <a href="{{route('industry.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3>{{count($blogs)}}</h3>

                                <p>Blogs</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-sticky-note-o"></i>
                            </div>
                            <a href="{{route('blogs.bloglist')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                @else
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>{{count($orders)}}</h3>

                                <p>My Orders</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            <a href="{{route('orders.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endrole
            </div>
            <!-- /.row -->


        </section>
        <!-- /.content -->
    </div>
@endsection
