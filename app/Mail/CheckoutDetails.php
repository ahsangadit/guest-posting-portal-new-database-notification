<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CheckoutDetails extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data , $data2)
    {
        $this->data=$data;
        $this->data2=$data2;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('guest-posting-portal@hztech.biz')->subject('User Cart Detail')->markdown('backend.email.check_out')->with(['data'=>$this->data,'data2'=>$this->data2]);
    }
}
