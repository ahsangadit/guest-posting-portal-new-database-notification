<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class notification extends Model
{

    public $table = "notifications";
     protected $fillable = [ 'orders_id', 'type', 'notifiable_id', 'data', 'notifiable_type', 'read_at'];


    //  public function order()
    //  {
    //      return $this->BelongsTo('App\orders');
    //  }

}
