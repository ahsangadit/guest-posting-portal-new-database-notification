<?php

namespace App;
 use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;

use Illuminate\Database\Eloquent\Model;

class orders extends Model
{
    public $timestamps = true;

    use Notifiable;

    public function user()
    {
        return $this->BelongsTo('App\User');
    }

    // public function notifications()
    // {
    //     return $this->hasOne('App\notification');
    // }
}
